## Vector Wood Pinball

<img src="/asset/Excalidraw.png" width="350">

Materials :

* A screen with a solid glass
* An android device (Raspberry PI 4 or Odroid)
* Three arcade buttons and the controller
* Câbles
* A power alimentation (depends on the screen and android device)
* A USB panel mounting bracket
* Wood
* Tools
* Time and patience

When the Android OS is installed, use a USB drive to install F-Droid (https://f-droid.org/F-Droid.apk)
And add Vector Pinball (https://github.com/dozingcat/Vector-Pinball) and Launch on boot to automattically launch the pinball app https://f-droid.org/packages/news.androidtv.launchonboot/.
Add also Power App to be able to power off with a key.

Don't forget to desactivate the stanby mode in the Android's settings.


The keys usable with a keyboard in Vector Pinball are:

- UP / DOWN : Navigate in the menus
- ENTER / SPACE BAR : Select
- ESC : Going Back (Menus), and going back to the home screen of android
- LEFT / RIGHT : Launch ball, activate left and right flipper

In my case i use a Raspberry PI 4 Model B
With LineageOS Android 12 from https://konstakang.com/devices/rpi4/LineageOS19/
Then i installed F-Droid from a USB key with the APK then connect with WIFI
As Launch on Boot don't work with this build from Konstakang i am using AutoStart Manager
Install VNC server in the Raspberry PI settings for remote access


